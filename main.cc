import helloworld;
import std;

void print(const std::string& msg) {
    std::cout << msg;
}

int main() {
    helloworld:HelloWorld h;

    std::string s = h.hello();
    std::cout << s << '\n';

    print("no std::print() yet\n");

    // Leaked pointer
    char *test = (char*)malloc(10);

    // Out of bounds access
    // cout << test[11];

    return 0;
}
