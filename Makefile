USE_CLANG := YES
USE_LTO := YES

BUILD_DIR := build
PREBUILT_DIR := $(BUILD_DIR)/prebuilt
DIRS := $(BUILD_DIR) $(PREBUILT_DIR)
APP := hellomodules

all: $(DIRS) $(APP)


ifdef USE_LTO
# Crashes with gcc13
LTO_CFLAGS := -flto
LTO_LDFLAGS :=
else
LTO_CFLAGS := -ffunction-sections -fdata-sections
LTO_LDFLAGS := -Wl,--gc-sections
endif


ifdef USE_CLANG

CXX := clang++
CXXFLAGS := \
	$(LTO_CFLAGS) \
	-std=c++2b -stdlib=libc++ \
	-MMD -MP \
	-fimplicit-modules -fimplicit-module-maps \
	-fprebuilt-module-path=$(PREBUILT_DIR) \
	-fmodules-cache-path=$(PREBUILT_DIR)
#	-fmodule-map-file=/usr/include/c++/v1/module.modulemap
#	-fbuiltin-module-map

MODFLAGS := -Xclang -emit-module-interface

else # GCC

CXX := g++
CXXFLAGS := \
	$(LTO_CFLAGS) \
	-std=c++23 \
	-MMD -MP \
	-fmodules-ts -fsanitize=leak,address
# crashes gcc 13: -fsanitize=undefined,leak,address
MODFLAGS :=

# libstdc++ 13 does not come with pre-built modules
std:
	$(CXX) $(CXXFLAGS) -fmodule-only -fmodule-header=system -x c++-system-header format
	$(CXX) $(CXXFLAGS) -fmodule-only -fmodule-header=system -x c++-system-header iostream
	$(CXX) $(CXXFLAGS) -fmodule-only -fmodule-header=system -x c++-system-header string
	$(CXX) $(CXXFLAGS) -fmodule-only -c std.cc -o build/prebuilt/std.pcm

endif

SRCS = main.cc helloworld.cc unused.cc
MODS = helloworld

OBJS = $(patsubst %.cc, $(BUILD_DIR)/%.o, $(SRCS))
PCMS = $(patsubst %, $(PREBUILT_DIR)/%.pcm, $(MODS))

clean:
	rm -f $(APP) $(OBJS) $(PCMS)
	rm -rf $(PREBUILT_DIR) gcm.cache

$(DIRS):
	mkdir -p $(DIRS)

$(APP): $(OBJS) $(PCMS)
	$(CXX) -Wl,--dependency-file="$(BUILD_DIR)/$(APP).d" $(LTO_LDFLAGS) $(CXXFLAGS) -o $@ $(OBJS)

$(BUILD_DIR)/%.o: %.cc $(PCMS)
	$(CXX) $(CXXFLAGS) $(LTO_CFLAGS) -o $@ -c $<

$(PREBUILT_DIR)/%.pcm: %.cc
	$(CXX) $(MODFLAGS) $(CXXFLAGS) $(LTO_CFLAGS) -o $@ -c $<
