module;

export module helloworld;

import std;

export class HelloWorld {
public:
    std::string hello() {
        std::cout << "hello, ";
        std::string s = "world!";
        return s;
    }
};
